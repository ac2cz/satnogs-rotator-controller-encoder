# SatNOGS Rotator Controller Encoder

Electronics designs for [SatNOGS Rotator](https://gitlab.com/librespacefoundation/satnogs/satnogs-rotator).

Repository includes all source files of PCB's for the SatNOGS rotator controller encoder assembly.

Firmware can be found on [satnogs-rotator-firmware](https://gitlab.com/librespacefoundation/satnogs/satnogs-rotator-firmware).

KiCAD symbols and footprints are placed in [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib)

## Documentation

More information can be found in our [wiki](https://wiki.satnogs.org/SatNOGS_Rotator_Controller).

## Repository policy

SatNOGS hardware repositories only track source design files. All needed derivative files (e.g. stl, grb etc) for production are created per release, packaged in an archive and uploaded linked to a  [release](https://gitlab.com/librespacefoundation/satnogs/satnogs-rotator-controller-encoder/tags).

Master branch is most times under active development, so expect things to break. For production ready and previous releases source files check tags.

## Contribute

The main repository lives on [Gitlab](https://gitlab.com/librespacefoundation/satnogs/satnogs-rotator-controller-encoder) and all Merge Request should happen there.

## License

&copy; 2014-2020 [Libre Space Foundation](http://libre.space).

Licensed under the [CERN OHLv1.2](LICENSE).
